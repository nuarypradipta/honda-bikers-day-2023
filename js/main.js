const main = () => {
  const second = 1000
  const minute = second * 60
  const hour = minute * 60
  const day = hour * 24

  const EVENTDATE = new Date('November 1, 2023, 19:00:00')

  const countDown = new Date(EVENTDATE).getTime()
  const x = setInterval(() => {

    const now = new Date().getTime()
    const distance = countDown - now

    document.getElementById("days").innerText = Math.floor(distance / day)
    document.getElementById("hours").innerText = Math.floor((distance % day) / (hour))
    document.getElementById("minutes").innerText = Math.floor((distance % hour) / (minute))
    document.getElementById("seconds").innerText = Math.floor((distance % minute) / second)

    //delay in milliseconds
  }, 0)
}

main();

$('.btn-toggle').on('click', function () {
  $('.mobile-nav').addClass('show');
  $('.overlay-mobile').addClass('show');
});
$('.btn-close-nav').on('click', function () {
  $('.mobile-nav').removeClass('show');
  $('.overlay-mobile').removeClass('show');
});

var swiper = new Swiper(".activity", {
  loop: true,
  autoplay: {
    delay: 1500,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    767: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
  }
});

var swiper = new Swiper(".partner-gold", {
  slidesPerView: 'auto',
  autoplay: {
    delay: 2500,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 10,
      loop: true,
    },
    767: {
      slidesPerView: 2,
      spaceBetween: 10,
      loop: true,
    },
    1024: {
      loop: true,
      slidesPerView: 3,
      spaceBetween: 10,
    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 10,
    },
  }
});

var swiper = new Swiper(".partner-silver", {
  slidesPerView: 'auto',
  autoplay: {
    delay: 2500,
  },
  breakpoints: {
    320: {
      slidesPerView: 2,
      spaceBetween: 10,
      loop: true,
    },
    767: {
      slidesPerView: 3,
      spaceBetween: 10,
      loop: true,
    },
    1024: {
      loop: true,
      slidesPerView: 4,
      spaceBetween: 10,
    },
    1200: {
      slidesPerView: 6,
      spaceBetween: 10,
    },
  }
});

var swiper = new Swiper(".partner-bronze", {
  slidesPerView: 'auto',
  autoplay: {
    delay: 2500,
  },
  breakpoints: {
    320: {
      slidesPerView: 3,
      spaceBetween: 10,
      loop: true,
    },
    767: {
      slidesPerView: 6,
      spaceBetween: 10,
      loop: true,
    },
    1024: {
      loop: true,
      slidesPerView: 8,
      spaceBetween: 10,
    },
    1200: {
      slidesPerView: 12,
      spaceBetween: 10,
    },
  }
});

var swiper = new Swiper(".testimonial", {
  slidesPerView: 1,
  loop: true,
  spaceBetween: 0,
  autoplay: false,
  navigation: {
    nextEl: "#testi.swiper-button-next",
    prevEl: "#testi.swiper-button-prev",
  },
});


$('#gallery').each(function () {
  $(this).magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true
    }
  });
});
